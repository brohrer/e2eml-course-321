import numpy as np
import conv_one_d_viz as conv1d
import cos_one_d_viz as cos1d


np.random.seed(67)

'''
signal = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
kernel = [.6, 1, .3]
conv1d.illustrate(signal, kernel, "aa_copy")

signal = [0, .5, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0]
kernel = [.6, 1, .3]
conv1d.illustrate(signal, kernel, "bb_copy")

signal = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
kernel = [1, .6, .36, .2, .1]
conv1d.illustrate(signal, kernel, "cc_decay")

signal = [0, .5, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0]
kernel = [1, .6, .36, .2, .1]
conv1d.illustrate(signal, kernel, "dd_copy")

signal = np.random.uniform(low=-1, high=1, size=20)
kernel = [.1, .2, .6, .9, 1, .9, .6, .2, .1]
conv1d.illustrate(signal, kernel, "ee_smooth")

signal = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
kernel = [.1, .2, .6, .9, 1, .9, .6, .2, .1]
conv1d.illustrate(signal, kernel, "ff_smooth")

signal = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
kernel = [.1, .2, .6, .9, 1, .9, .6, .2, .1]
conv1d.illustrate(signal, kernel, "gg_smooth")

signal = np.random.uniform(low=-1, high=1, size=20)
kernel = [.1, .2, .6, .9, 1, .9, .6, .2, .1]
conv1d.illustrate(signal, kernel, "hh_smooth")

signal = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
kernel = [1, -1]
conv1d.illustrate(signal, kernel, "ii_edge")

signal = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
kernel = [1, 1, -1, -1]
conv1d.illustrate(signal, kernel, "jj_soft_edge")

signal = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
kernel = [1, 0, -1]
conv1d.illustrate(signal, kernel, "kk_dont_care")

signal = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
kernel = [1, 0, 0, 0, -1]
conv1d.illustrate(signal, kernel, "ll_dont_care")

signal = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]
kernel = [1, .4, 0, -.4, -1]
conv1d.illustrate(signal, kernel, "mm_dont_care")

signal = [0, 0, 0, 0, 0, .2, .5, .8, 1, 1, .8, .5, .2, 0, 0, 0, 0, 0]
kernel = [1, -.4, 0, .4, -1]
conv1d.illustrate(signal, kernel, "nn_sharpen")

signal = [
    0, 0, 0, 0, 0, 0, .1, .3, .5,
    0, 0, 0, 0, 0, 0, .5, .3, .1,
    0, 0, 0, 0, 0, 0, 1, 1, 1,
    0, 0, 0, 0, 0, 0, .5, .5, .5,
    0, 0, 0, 0, 0, 0]
kernel = [.1, .3, .5]
conv1d.illustrate(signal, kernel, "oo_not_match")

signal = [
    0, 0, 0, 0, 0, 0, 0, 1, -1, -1, 1,
    0, 0, 0, 0, 0, 0, 0, -1, 1, 1, -1,
    0, 0, 0, 0, 0, 0, 0, 1, 1, -1, -1,
    0, 0, 0, 0, 0, 0, 0, -1, 1, -1, 1,
    0, 0, 0, 0, 0, 0, 0]
kernel = [1, -1, -1, 1]
conv1d.illustrate(signal, kernel, "pp_match")

signal = [
    0, 0, 0, 0, 0, 0, .1, .3, .5,
    0, 0, 0, 0, 0, 0, .5, .3, .1,
    0, 0, 0, 0, 0, 0, 1, 1, 1,
    0, 0, 0, 0, 0, 0, .5, .5, .5,
    0, 0, 0, 0, 0, 0]
kernel = [.1, .3, .5]
# cos1d.illustrate(signal, kernel, "qq_cos_match")
# cos1d.illustrate(signal, kernel, "rr_cos_match_power")  # with cos sim power
cos1d.illustrate(signal, kernel, "ss_scd")  # with q added
'''

signal = [
    0, 0, 0, 0, 0, 0, .2, .6, 1,
    0, 0, 0, 0, 0, 0, -.2, -.6, -1,
    0, 0, 0, 0, 0, 0, .1, .3, .5,
    0, 0, 0, 0, 0, 0, .01, .03, .05,
    0, 0, 0, 0, 0, 0]
kernel = [.2, .6, 1]
cos1d.illustrate(signal, kernel, "tt_scd_scale")  # with q added

