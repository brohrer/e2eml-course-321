import os
import numpy as np
from cottonwood.core.blocks.activation import Logistic, ReLU, TanH
from cottonwood.core.blocks.conv1d import Conv1D
from cottonwood.core.blocks.linear import Linear
from cottonwood.core.blocks.loss import MeanSquareLoss
from cottonwood.core.blocks.operations import Difference, Flatten, OneHot
from cottonwood.core.blocks.pooling import MaxPool1D
from cottonwood.core.blocks.structure import Structure
import cottonwood.core.toolbox as tb
import cottonwood.examples.convnet.conv1d_viz as conv_viz
import cottonwood.examples.simulation.visualize_structure as struct_viz
from cottonwood.core.logger import ValueLogger
from cottonwood.experimental.blocks.normalization \
    import OnlineBatchNormalization as BatchNormalization
from cottonwood.core.optimizers import Momentum
from ponderosa.optimizers_parallel import EvoPowell
from ecg_data_block import TrainingData, TuningData, TestingData


def main():
    """
    Build an optimization task by creating a `conditions` dictionary.
    Each key is a condition you want to vary. It can be a number, a string,
    a function, or a class - whatever you'd like to vary.
    Each value is a list of the values you's like to try out for that key.
    """
    # Optimize the learning rate of the two layers separately
    conditions = {
        "ConvActivation": [Logistic, ReLU, TanH],
        "learning_rate_conv": list(10 ** np.linspace(-.5, .5, 3)),
        "learning_rate_linear": list(10 ** np.linspace(-4.5, -3.5, 3)),
    }

    # Choose your optimization algorithm and run its optimize() method.
    optimizer = EvoPowell()
    lowest_error, best_condition, results_logfile = optimizer.optimize(
        evaluate, conditions, verbose=True)

    print(
        "All done! The data on each condition evaluated, and its error\n"
        + f"are stored in {results_logfile}.")


def evaluate(
    verbose=False,
    **kwargs,
):
    n_runs = 5
    tuning_losses = []
    for i_run in range(n_runs):
        if verbose:
            print(f"Run {i_run + 1} of {n_runs}")
        _, tuning_loss, _ = train(verbose=verbose, **kwargs)
        tuning_losses.append(tuning_loss)
    return np.median(tuning_losses)


def train(
    ConvActivation=None,
    learning_rate_conv=1,
    learning_rate_linear=1e-4,
    minibatch_size_conv=32,
    minibatch_size_linear=1,
    verbose=False,
    **kwargs,
):
    reports_dir = os.path.join("reports", tb.date_string())
    if verbose:
        os.makedirs(reports_dir, exist_ok=True)

        msg = f"""

    Training convolutional neural network on the ECG data set.
    Look for documentation and visualizations
    in the {reports_dir} directory.

    """
        print(msg)

    kernel_size = 5
    n_kernels = 15
    n_training_iter = int(3e5)
    n_tuning_iter = int(3e4)
    n_report_interval = int(1e4)
    n_viz_interval = int(1e5)

    classifier = Structure()
    classifier.add(TrainingData(), "training_data")

    # Add blocks for the classification branch of the network
    classifier.add(Conv1D(
        kernel_size=kernel_size,
        n_kernels=n_kernels,
        optimizer=Momentum(
            learning_rate=learning_rate_conv,
            minibatch_size=minibatch_size_conv,
        ),
    ), "convolution_0")
    classifier.add(ConvActivation(), "activation_0")
    classifier.add(MaxPool1D(), "max_pool_0")
    classifier.add(Flatten(), "flatten")
    classifier.add(Linear(
        4,
        optimizer=Momentum(
            learning_rate=learning_rate_linear,
            minibatch_size=minibatch_size_linear,
        ),
    ), "linear")
    classifier.add(Logistic(), "logistic")

    # Add blocks for the ground truth comparison branch
    classifier.add(OneHot(4), "one_hot")
    classifier.add(Difference(), "difference")
    classifier.add(MeanSquareLoss(), "mean_sq_loss")

    # Create all the connections between the blocks
    classifier.connect("training_data", "convolution_0", i_port_tail=0)
    classifier.connect("training_data", "one_hot", i_port_tail=1)

    classifier.connect("convolution_0", "activation_0")
    classifier.connect("activation_0", "max_pool_0")
    classifier.connect("max_pool_0", "flatten")
    classifier.connect("flatten", "linear")
    classifier.connect("linear", "logistic")

    classifier.connect("logistic", "difference", i_port_head=0)
    classifier.connect("one_hot", "difference", i_port_head=1)
    classifier.connect("difference", "mean_sq_loss")

    loss_logger = ValueLogger(
        value_name="loss",
        log_scale=True,
        report_min=-3,
        report_max=0,
        reports_path=reports_dir,
        reporting_bin_size=n_report_interval,
        verbose=verbose,
    )

    # Execute the training loop
    for i_iter in range(n_training_iter):
        classifier.forward_pass()
        classifier.backward_pass()
        loss_logger.log_value(classifier.blocks["mean_sq_loss"].loss)
        if verbose:
            if (i_iter + 1) % n_viz_interval == 0:
                conv_viz.render(
                    classifier.blocks["convolution_0"],
                    reports_dir,
                    f"conv_0_{i_iter + 1:07}.png")
    if verbose:
        tb.summarize(classifier, reports_dir=reports_dir)
        struct_viz.render(classifier, reports_dir)

    classifier.remove("training_data")
    classifier.add(TuningData(), "tuning_data")
    classifier.connect("tuning_data", "convolution_0", i_port_tail=0)
    classifier.connect("tuning_data", "one_hot", i_port_tail=1)

    # Execute the loop evaluating performance on the tuning data
    for i_iter in range(n_tuning_iter):
        classifier.forward_pass()
        loss_logger.log_value(classifier.blocks["mean_sq_loss"].loss)

    tuning_loss = np.log10(np.mean(
        loss_logger.value_history[n_training_iter:]))
    return classifier, tuning_loss, loss_logger.value_history


if __name__ == "__main__":
    main()
