"""
Generate the equations describing one dimensional convolution
and its derivative.
"""
import os
# import numpy as np
import matplotlib
import matplotlib.pyplot as plt

plt.switch_backend("agg")
matplotlib.rcParams['text.usetex'] = True


# parameter dictionary
# All dimensions are in units of centimeters
hgt = 9  # figure height
wid = 16  # figure width
hbrd = 2  #  horizontal border width
vbrd = 1  # vertical border width
eq_spacing = .8  # vertical spacing of equations, including text height
eq_indent = 2  # how much to indent broken equation lines
mathsize = 16  # font size for math text

# Valid Matplotlib colors. More examples here:
# https://e2eml.school/matplotlib_lines.html#color
canvas_color = "#e1ddbf"  # tan
math_color = "#04253a"  # dark blue

dpi = 300  # Image resolution, dots per inch
eqs_dir = "eqs"

# Ensure that the necessary directories exist
try:
    os.mkdir(eqs_dir)
except FileExistsError:
    pass


def get_empty_frame(
    fig_width=16,  # centimeters
    fig_height=9,  # centimeters
    canvas_color="white",
    border_color="black",
):
    """
    Generate a blank canvas on which to draw
    """
    # figsize expects inches. Convert from centimeters.
    fig = plt.figure(figsize=(fig_width / 2.54, fig_height / 2.54))

    # Cover the entire figure with an Axes object
    ax = fig.add_axes((0, 0, 1, 1))

    # Ensure that 1 unit in the Axes is 1 centimeter in the final image
    ax.set_xlim(0, fig_width)
    ax.set_ylim(0, fig_height)

    ax.set_facecolor(canvas_color)

    # Clean up the axes
    ax.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    ax.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    ax.spines["top"].set_color(border_color)
    ax.spines["bottom"].set_color(border_color)
    ax.spines["left"].set_color(border_color)
    ax.spines["right"].set_color(border_color)
    ax.spines["top"].set_linewidth(4)
    ax.spines["bottom"].set_linewidth(4)
    ax.spines["left"].set_linewidth(4)
    ax.spines["right"].set_linewidth(4)

    return fig, ax

# Definitions
fig_height = hgt / 2
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)

eq1 = r"$x = [x_0, x_1, x_2, \ldots , x_{m - 1} ]$"
x = hbrd
y = fig_height - vbrd
ax.text(
    x, y,
    eq1,
    horizontalalignment="left",
    verticalalignment="center",
    color=math_color,
    fontsize=mathsize,
)

eq2 = r"$w = [w_0, w_1, w_2, \ldots , w_{n - 1} ]$"
y -= eq_spacing
ax.text(
    x, y,
    eq2,
    horizontalalignment="left",
    verticalalignment="center",
    color=math_color,
    fontsize=mathsize,
)
eq3 = r"$y = [y_0, y_1, y_2, \ldots , y_{m - 1} ]$"
y -= eq_spacing
ax.text(
    x, y,
    eq3,
    horizontalalignment="left",
    verticalalignment="center",
    color=math_color,
    fontsize=mathsize,
)

fig.savefig(os.path.join(eqs_dir, "defs_1_eq.png"), dpi=dpi)

fig_height = hgt / 2
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)

eq1 = r"$x = [x_0, x_1, x_2, \ldots , x_{m - 1} ]$"
x = hbrd
y = fig_height - vbrd
ax.text(
    x, y,
    eq1,
    horizontalalignment="left",
    verticalalignment="center",
    color=math_color,
    fontsize=mathsize,
)

eq2 = r"$w = [w_{-p}, w_{-p+1}, \ldots,  w_0, \ldots , w_{p - 1}, w_{p} ]$"
y -= eq_spacing
ax.text(
    x, y,
    eq2,
    horizontalalignment="left",
    verticalalignment="center",
    color=math_color,
    fontsize=mathsize,
)
eq3 = r"$y = [y_0, y_1, y_2, \ldots , y_{m - 1} ]$"
y -= eq_spacing
ax.text(
    x, y,
    eq3,
    horizontalalignment="left",
    verticalalignment="center",
    color=math_color,
    fontsize=mathsize,
)

fig.savefig(os.path.join(eqs_dir, "defs_2_eq.png"), dpi=dpi)
plt.close()

def plot_eq(ax, x, y, eq):
    ax.text(
        x, y,
        eq,
        horizontalalignment="left",
        verticalalignment="center",
        color=math_color,
        fontsize=mathsize,
    )

# Convolution, longhand 
fig_height = 9
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - vbrd
plot_eq(
    ax, x, y,
    eq=r"$y_0 = x_0 w_0 + x_1 w_{-1} + \ldots + x_p w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_1 = x_0 w_1 + x_1 w_{0} + x_2 w_{-1} + \ldots + x_{p + 1} w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_2 = x_0 w_2 + x_1 w_{1} + \ldots + x_{p + 2} w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_p = x_0 w_p + x_1 w_{p-1} + \ldots + x_p w_{0} + \ldots$",
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_p w_{0} + \ldots + x_{2p} w_{-p}$",
)


y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_{p + 1} = x_1 w_p + x_2 w_{p-1} + \ldots$",
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_{p+1} w_{0} + \ldots + x_{2p + 1} w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

fig.savefig(os.path.join(eqs_dir, "conv_long_1_eq.png"), dpi=dpi)

# Convolution, with summations
fig_height = 9
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$y_0 = \displaystyle\sum_{k = -p}^{p} x_{-k} w_k$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_1 = \displaystyle\sum_{k = -p}^{p} x_{1-k} w_k$",
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_m = \displaystyle\sum_{k = -p}^{p} x_{m-k} w_k$",
)


fig.savefig(os.path.join(eqs_dir, "conv_sum_eq.png"), dpi=dpi)

# Convolution
fig_height = 4
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$y_j = \displaystyle\sum_{k = -p}^{p} x_{j-k} w_k$",
)

fig.savefig(os.path.join(eqs_dir, "conv_1_eq.png"), dpi=dpi)

# Input gradient
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x} = \frac{\partial \mathcal{L}}{\partial y} \frac{\partial y}{\partial x}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x_i} = \frac{\partial \mathcal{L}}{\partial y} \frac{\partial y}{\partial x_i}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x_i} = \sum_{j=0}^{m-1} \frac{\partial \mathcal{L}}{\partial y_j} \frac{\partial y_j}{\partial x_i}$",
)

fig.savefig(os.path.join(eqs_dir, "input_grad_1_eq.png"), dpi=dpi)

# Input gradient by element
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 1.5 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$y_j = \displaystyle\sum_{k = -p}^{p} x_{j-k} w_k$",
)

y -= 2 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_j = x_{j-p}w_p + x_{j-p+1}w_{p-1} + x_{j-p+2}w_{p-2} + \ldots$"
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_{j+p-2}w_{-p+2} + x_{j+p-1}w_{-p+1} + x_{j+p}w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_{j+1} = x_{j-p+1}w_p + x_{j-p+2}w_{p-1} + x_{j-p+3}w_{p-2} + \ldots$"
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_{j+p-1}w_{-p+2} + x_{j+p}w_{-p+1} + x_{j+p+1}w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_{j+2} = x_{j-p+2}w_p + x_{j-p+3}w_{p-1} + x_{j-p+4}w_{p-2} + \ldots$"
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_{j+p}w_{-p+2} + x_{j+p+1}w_{-p+1} + x_{j+p+2}w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

fig.savefig(os.path.join(eqs_dir, "input_grad_2_eq.png"), dpi=dpi)


# Input gradient by element, pairwise derivatives
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - vbrd
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_j}{\partial x_{j-p}} = w_{p}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial x_{j-p+1}} = w_{p-1}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial x_{j-p+2}} = w_{p-2}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_j}{\partial x_{j+p-2}} = w_{-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial x_{j+p-1}} = w_{-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial x_{j+p}} = w_{-p}$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+1}}{\partial x_{j-p+1}} = w_{p}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial x_{j-p+2}} = w_{p-1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial x_{j-p+3}} = w_{p-2}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+1}}{\partial x_{j+p-1}} = w_{-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial x_{j+p}} = w_{-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial x_{j+p+1}} = w_{-p}$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+2}}{\partial x_{j-p+2}} = w_{p}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial x_{j-p+3}} = w_{p-1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial x_{j-p+4}} = w_{p-2}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+2}}{\partial x_{j+p}} = w_{-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial x_{j+p+1}} = w_{-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial x_{j+p+2}} = w_{-p}$"
    ),
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

fig.savefig(os.path.join(eqs_dir, "input_grad_3_eq.png"), dpi=dpi)


# Input gradient by element, pairwise derivatives, grouped by input
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - vbrd
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+p}}{\partial x_{j}} = w_{p}$"
        + r"$\hskip 6mm \frac{\partial y_{j+p-1}}{\partial x_{j}} = w_{p-1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+p-2}}{\partial x_{j}} = w_{p-2}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j-p+2}}{\partial x_{j}} = w_{-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_{j-p+1}}{\partial x_{j}} = w_{-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_{j-p}}{\partial x_{j}} = w_{-p}$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+p+1}}{\partial x_{j+1}} = w_{p}$"
        + r"$\hskip 6mm \frac{\partial y_{j+p}}{\partial x_{j+1}} = w_{p-1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+p-1}}{\partial x_{j+1}} = w_{p-2}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j-p+3}}{\partial x_{j+1}} = w_{-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_{j-p+2}}{\partial x_{j+1}} = w_{-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_{j-p+1}}{\partial x_{j+1}} = w_{-p}$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+p+2}}{\partial x_{j+2}} = w_{p}$"
        + r"$\hskip 6mm \frac{\partial y_{j+p+1}}{\partial x_{j+2}} = w_{p-1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+p}}{\partial x_{j+2}} = w_{p-2}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j-p+4}}{\partial x_{j+2}} = w_{-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_{j-p+3}}{\partial x_{j+2}} = w_{-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_{j-p+2}}{\partial x_{j+2}} = w_{-p}$"
    ),
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

fig.savefig(os.path.join(eqs_dir, "input_grad_4_eq.png"), dpi=dpi)


# Input gradient by element, pairwise derivatives, grouped by input, simplified
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=(
        r"$\displaystyle \frac{\partial y_{i+k}}{\partial x_{i}} = w_{k}$"
    ),
)
y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x_i} = \sum_{k=-p}^{p} \frac{\partial \mathcal{L}}{\partial y_{i+k}} \frac{\partial y_{i+k}}{\partial x_i}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x_i} = \sum_{k=-p}^{p} \frac{\partial \mathcal{L}}{\partial y_{i+k}} w_k$",
)

fig.savefig(os.path.join(eqs_dir, "input_grad_5_eq.png"), dpi=dpi)

# Input gradient by element, reversed kernel
fig_height = hgt *.7
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x_i} = \sum_{k=-p}^{p} \frac{\partial \mathcal{L}}{\partial y_{i-k}} \overleftarrow{w}_k$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x} = \frac{\partial \mathcal{L}}{\partial y} \ast \overleftarrow{w}$",
)
fig.savefig(os.path.join(eqs_dir, "input_grad_6_eq.png"), dpi=dpi)

# Parameter gradient
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w} = \frac{\partial \mathcal{L}}{\partial y} \frac{\partial y}{\partial w}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w_k} = \frac{\partial \mathcal{L}}{\partial y} \frac{\partial y}{\partial w_k}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w_k} = \sum_{j=0}^{m-1} \frac{\partial \mathcal{L}}{\partial y_j} \frac{\partial y_j}{\partial w_k}$",
)

fig.savefig(os.path.join(eqs_dir, "param_grad_1_eq.png"), dpi=dpi)

# Parameter gradient by element
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 1.5 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$y_j = \displaystyle\sum_{k = -p}^{p} x_{j-k} w_k$",
)

y -= 2 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_j = x_{j-p}w_p + x_{j-p+1}w_{p-1} + x_{j-p+2}w_{p-2} + \ldots$"
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_{j+p-2}w_{-p+2} + x_{j+p-1}w_{-p+1} + x_{j+p}w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_{j+1} = x_{j-p+1}w_p + x_{j-p+2}w_{p-1} + x_{j-p+3}w_{p-2} + \ldots$"
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_{j+p-1}w_{-p+2} + x_{j+p}w_{-p+1} + x_{j+p+1}w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$y_{j+2} = x_{j-p+2}w_p + x_{j-p+3}w_{p-1} + x_{j-p+4}w_{p-2} + \ldots$"
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$+ x_{j+p}w_{-p+2} + x_{j+p+1}w_{-p+1} + x_{j+p+2}w_{-p}$",
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

fig.savefig(os.path.join(eqs_dir, "param_grad_2_eq.png"), dpi=dpi)

# parameter gradient by element, pairwise derivatives
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - vbrd
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_j}{\partial w_{p}} =  x_{j-p}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial w_{p-1}} = x_{j-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial w_{p-2}}= x_{j-p+2}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_j}{\partial w_{-p+2}} = x_{j+p-2}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial w_{-p+1}} = x_{j+p-1}$"
        + r"$\hskip 6mm \frac{\partial y_j}{\partial w_{-p}} = x_{j+p}$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+1}}{\partial w_{p}} = x_{j-p+1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial w_{p-1}} = x_{j-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial w_{p-2}} = x_{j-p+3}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+1}}{\partial w_{-p+2}} = x_{j+p-1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial w_{-p+1}} = x_{j+p}$"
        + r"$\hskip 6mm \frac{\partial y_{j+1}}{\partial w_{-p}} = x_{j+p+1}$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+2}}{\partial w_{p}} = x_{j-p+2}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial w_{p-1}} = x_{j-p+3}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial w_{p-2}} = x_{j-p+4}$"
        + r"$\hskip 6mm \ldots$"
    ),
)

y -= 1.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=(
        r"$\frac{\partial y_{j+2}}{\partial w_{-p+2}} = x_{j+p}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial w_{-p+1}} = x_{j+p+1}$"
        + r"$\hskip 6mm \frac{\partial y_{j+2}}{\partial w_{-p}} = x_{j+p+2}$"
    ),
)

y -= eq_spacing
plot_eq(
    ax, x + eq_indent, y,
    eq=r"$\vdots$",
)

fig.savefig(os.path.join(eqs_dir, "param_grad_3_eq.png"), dpi=dpi)


# Parameter gradient by element, pairwise derivatives, grouped by input, simplified
fig_height = hgt
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=(
        r"$\displaystyle \frac{\partial y_{j}}{\partial w_{k}} = x_{j-k}$"
    ),
)
y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w_k} = \sum_{j=0}^{m-1} \frac{\partial \mathcal{L}}{\partial y_{j}} \frac{\partial y_{j}}{\partial w_k}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w_k} = \sum_{j=0}^{m-1} \frac{\partial \mathcal{L}}{\partial y_{j}} x_{j-k}$",
)

fig.savefig(os.path.join(eqs_dir, "param_grad_4_eq.png"), dpi=dpi)


# parameter gradient by element, reversed kernel
fig_height = hgt *.7
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w_k} = \sum_{j=0}^{m-1} \frac{\partial \mathcal{L}}{\partial y_{j}} \overleftarrow{x_{k-j}}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w} = \frac{\partial \mathcal{L}}{\partial y} \ast \overleftarrow{x}$",
)
fig.savefig(os.path.join(eqs_dir, "param_grad_5_eq.png"), dpi=dpi)


# main equations, covering multiple channels
fig_height = 9
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 2 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$y_j = \displaystyle\sum_{c=0}^{n_c - 1}\sum_{k = -p}^{p} x_{c,j-k} w_{c,k} \hspace{1cm}  \mbox{convolution}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x_{c,i}} = \sum_{k=-p}^{p} \frac{\partial \mathcal{L}}{\partial y_{i+k}} w_{c,k} \hspace{1cm} \mbox{input gradient}$",
)

y -= 3 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w_{c,k}} = \sum_{j=0}^{m-1} \frac{\partial \mathcal{L}}{\partial y_{j}} x_{c, j-k} \hspace{1cm} \mbox{parameter gradient}$",
)
fig.savefig(os.path.join(eqs_dir, "conv_multichannel_eq.png"), dpi=dpi)

# main equations, covering multiple channels, including bias
fig_height = 9
fig, ax = get_empty_frame(
    fig_width=wid,
    fig_height=fig_height,
    canvas_color=canvas_color,
    border_color=canvas_color,
)
x = hbrd
y = fig_height - 1.5 * vbrd
plot_eq(
    ax, x, y,
    eq=r"$y_j = b_j + \displaystyle\sum_{c=0}^{n_c - 1}\sum_{k = -p}^{p} x_{c,j-k} w_{c,k}\hspace{1cm}  \mbox{convolution}$",
)

y -= 2.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial x_{c,i}} = \sum_{k=-p}^{p} \frac{\partial \mathcal{L}}{\partial y_{i+k}} w_{c,k} \hspace{1cm} \mbox{input gradient}$",
)

y -= 2.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial w_{c,k}} = \sum_{j=0}^{m-1} \frac{\partial \mathcal{L}}{\partial y_{j}} x_{c, j-k} \hspace{1cm} \mbox{parameter gradient}$",
)

y -= 2.5 * eq_spacing
plot_eq(
    ax, x, y,
    eq=r"$\displaystyle \frac{\partial \mathcal{L}}{\partial b_j} = \frac{\partial \mathcal{L}}{\partial y_{j}} \hspace{1cm} \mbox{bias gradient}$",
)
fig.savefig(os.path.join(eqs_dir, "conv_bias_eq.png"), dpi=dpi)
