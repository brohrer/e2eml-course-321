import conv_one_d_stride_viz as conv1d


signal = [
    .8, .5, .9, -.1, .7, .6, 1, .3, .9, .4, -.3, .2, -.1,
    .4, -.2, -.8, -.6, -.9, .1, -.1, -.3, -1, -.8, -.5, -.9,
]
kernel = [.1, .3, .7, .9, 1, .9, .7, .3, .1]
conv1d.illustrate(signal, kernel, "xa_stride_1", stride=1)
conv1d.illustrate(signal, kernel, "xb_stride_2", stride=2)
conv1d.illustrate(signal, kernel, "xc_stride_3", stride=3)
conv1d.illustrate(signal, kernel, "xd_stride_4", stride=4)
