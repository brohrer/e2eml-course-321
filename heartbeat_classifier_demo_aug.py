"""
The Wikipedia ECG page
(https://en.wikipedia.org/wiki/Electrocardiography) really helps
to make sense out of the data. It gives just enough domain knowledge
to avoid doing anything really dumb here.

If you haven't already, install the Waveform Database
(https://github.com/MIT-LCP/wfdb-python) package for Python:

    python3 -m pip install wfdb

This is a library specifically tailored physiological data
of the format we'll be working with.

I've downloaded the data to a directory called "data" and unzipped it there.
The file "mit-bih-arrhythmia-database-1.0.0/mitdbdir/intro.html"
is as thorough and clear a data description as I've ever seen.
This dataset has been lovingly cared for.
"""
import os
import pickle as pkl
import numpy as np
import wfdb
from cottonwood.core.blocks.operations import Constant
from ecg_viz_mark_4 import make_ecg_plots


# The total amount of data to pull from each recording
N_SECONDS = 600


def main():
    """
    For now keep it simple and streamlined.
    Our analyses can be extended later to include a wider set of subjects,
    measurements, and categories.

    * Focus on 100-series subjects (not especially selected
          for pathological conditions).
    * Focus on MLII (modified limb lead II) signal only.
    * Ignore subjects 102, 104 (MLII not available)
    * Ignore subjects 114 (leads reversed)
    * Ignore subjects 112, 115 through 124 (recorded at twice real time)

    Class balance matters. We'll shoot for the roughly same number
    of training examples from each class.
    We can do some oversampling of smaller classes, but too much oversampling
    means we're just overfitting to a handful of examples. For now we'll
    limit ourselves to learning 'N', 'V', '/', and 'L'.
    We can always extend it to other classes later.
    count_labels(include_ids)

    Using
    include_ids = [100, 101, 103, 105, 106, 107, 108, 109, 111, 113]
    * 'N': 13,742 instances. Normal
    * 'L':  4,615 instances. Left bundle branch block
    * '/':  2,078 instances. Paced beat
    * 'V':    677 instances. Premature ventricular contraction
    """
    reports_dir = os.path.join("demo_results_aug")
    images_dir = os.path.join(reports_dir, "images")
    os.makedirs(images_dir, exist_ok=True)

    # Load the model and remove the parts we don't need for making predictions
    filename = "heartbeat_classifier_aug.pkl"
    with open(filename, "rb") as f:
        classifier = pkl.load(f)

    labels_by_index = classifier.blocks["one_hot"].get_labels()

    classifier.remove("one_hot")
    classifier.remove("logistic_copy")
    classifier.remove("difference")
    classifier.remove("mean_sq_loss")
    classifier.remove("hard_max")

    classifier.add(Constant(None), "input")
    classifier.connect("input", "convolution_0")

    data_dir = os.path.join("data", "mit-bih-arrhythmia-database-1.0.0")
    include_ids = [100, 101, 103, 105, 106, 107, 108, 109, 111, 113]

    beat_window = 500  # in milliseconds
    f_samp = 360  # sampling rate in Hz
    half_window = int(f_samp * beat_window / (2 * 1000))  # in samples

    for patient_id in include_ids:
        print(f"Rendering data for patient {patient_id}")
        signal, label_locs, labels = load_waves(patient_id, data_dir)
        # Find all the centerpoints to evaluate
        i_start_eval_center = half_window
        i_end_eval_center = signal.shape[1] - half_window - 1
        pt_spacing = 1
        eval_pts = np.arange(
            i_start_eval_center,
            i_end_eval_center + 1,
            pt_spacing)

        result_history = []
        for eval_pt in eval_pts:
            i_start_eval = eval_pt - half_window
            i_end_eval = eval_pt + half_window
            signal_snippet = signal[:, i_start_eval: i_end_eval + 1]

            # Manually set the input
            classifier.blocks["input"].result = signal_snippet
            classifier.forward_pass()
            result_history.append(classifier.blocks["logistic"].result.ravel())

        result_history = np.array(result_history).transpose()

        make_ecg_plots(
            signal,
            result_history,
            label_locs,
            labels,
            labels_by_index,
            patient_id,
            f_samp,
            half_window,
            pt_spacing,
            images_dir)


def load_waves(patient_id, data_dir):
    """
    Pre-load all the data.
    """
    signal = get_signal(patient_id, data_dir)[:, :N_SECONDS * 360]
    label_locs, labels = get_ann(patient_id, data_dir)
    return signal, label_locs, labels


def get_signal(pid, data_dir):
    signal, _ = wfdb.rdsamp(os.path.join(data_dir, str(pid)), channels=[0, 1])
    derivative_scale = 10
    d_signal = derivative_scale * (signal[2:, :] - signal[:-2, :]) / 2
    all_signals = np.concatenate((signal[1: -1, :], d_signal), axis=1)
    return all_signals.transpose()


def get_ann(pid, data_dir):
    ann = wfdb.rdann(os.path.join(data_dir, str(pid)), "atr")
    return ann.sample, ann.symbol


if __name__ == "__main__":
    main()
